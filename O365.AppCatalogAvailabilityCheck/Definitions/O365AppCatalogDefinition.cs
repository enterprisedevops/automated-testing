﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
//using OpenQA.Selenium.Interactions;
//using OpenQA.Selenium.Remote;
//using OpenQA.Selenium.Support.UI;
using System.Configuration;


namespace O365.AppCatalogAvailabilityCheck
{
	[Binding]
	public sealed class O365AppCatalogDefinition
	{

		#region Global variables
		string env = ConfigurationManager.AppSettings["Env"];
		string run = ConfigurationManager.AppSettings["Run"];
		IWebDriver driver;
		#endregion

		#region Initialization
		public bool StartUp()
		{

			Console.WriteLine("Scenarion -Browser testing- started");
			//DesiredCapabilities capabilities = new DesiredCapabilities();
			string browser = ConfigurationManager.AppSettings["Browser"];

			try
			{
				switch (browser)
				{
					
					case "Chrome":
						{
							
							driver = new ChromeDriver();
							break;
						}
                    case "Firefox":
                        {

                            driver = new FirefoxDriver();
                            break;
                        }


                    default:
						{
							Console.WriteLine("Browser Name not matching setting Chrome as default browser");
							driver = new ChromeDriver();							
							break;
						}

				}



				driver.Manage().Window.Maximize();
				PropertiesCollection.driver = driver;
				Console.WriteLine("Session Starts......");
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to start session....." + ex.Message);
				CloseBrowser(false);
				return false;
			}
		}

		public void CloseBrowser(bool result)
		{
			try
			{
				Console.WriteLine("Result-." + result);
				if (run == "SauceLabs")
				{
					if (driver != null)
					{
						((IJavaScriptExecutor)driver).ExecuteScript("sauce:job-result=" + (result ? "passed" : "failed"));

					}
					if (PropertiesCollection.driver != null)
						((IJavaScriptExecutor)PropertiesCollection.driver).ExecuteScript("sauce:job-result=" + (result ? "passed" : "failed"));
				}

				if (driver != null)
					driver.Quit();
				if (PropertiesCollection.driver != null)
					PropertiesCollection.driver.Quit();
			}
			catch { }
		}
		public void CloseBrowser()
		{
			if (driver != null)
				driver.Quit();
			if (PropertiesCollection.driver != null)
				PropertiesCollection.driver.Quit();

		}
        #endregion


        [Given(@"Open Microsoft Login Page")]
        public void GivenOpenMicrosoftLoginPage()
        {
            try
            {
                if (StartUp())
                {

                    Console.WriteLine("Opening url:" + Keywords.Constants["SITE_URL_Active"]);
                    driver.Navigate().GoToUrl(Keywords.Constants["SITE_URL_Active"]);
                   // Thread.Sleep(60 * 1000);
                    CommonUtilities.TakeScreenshot();


                }

            }
            catch (Exception ex)
            {
                CommonUtilities.TakeScreenshot();
                Console.WriteLine("Unable to open O365 application" + ex.Message);
                CloseBrowser(false);
                Assert.Fail();

            }
        }

        [Then(@"I Close Browser")]
        public void ThenICloseBrowser()
        {
            Thread.Sleep(1000);
            CloseBrowser();
        }


        [Then(@"Then I enter encrypted ""(.*)"" into ""(.*)"" field")]
        public void ThenThenIEnterEncryptedIntoField(string p0, string p1)
        {
            try
            {
                Thread.Sleep(1000);
                CommonUtilities.EnterEncryptedText(p1, p0);
                Thread.Sleep(1000);
                CommonUtilities.TakeScreenshot();

            }
            catch (Exception ex)
            {
                CommonUtilities.TakeScreenshot();
                Console.WriteLine("Unable to find element" + ex.Message);
                CloseBrowser(false);
                Assert.Fail();

            }
        }


        [Then(@"Then I enter ""(.*)"" into ""(.*)"" field")]
        public void ThenThenIEnterIntoField(string p0, string p1)
        {
            try
            {
                Thread.Sleep(1000);
                CommonUtilities.EnterText(p1, p0);
                Thread.Sleep(1000);
                CommonUtilities.TakeScreenshot();

            }
            catch (Exception ex)
            {
                CommonUtilities.TakeScreenshot();
                Console.WriteLine("Unable to find element" + ex.Message);
                CloseBrowser(false);
                Assert.Fail();

            }
        }

        [Then(@"I Open BPA App Catalog Page")]
        public void ThenIOpenBPAAppCatalogPage()
        {
            Console.WriteLine("Opening url:" + Keywords.Constants["BPA_SITE_URL_Active"]);
            driver.Navigate().GoToUrl(Keywords.Constants["BPA_SITE_URL_Active"]);
            // Thread.Sleep(60 * 1000);
            CommonUtilities.TakeScreenshot();
        }

        [Then(@"I check for ""(.*)""")]
		public void ThenICheckFor(string p0)
		{
			try
			{
				Thread.Sleep(10000);
				string welcome = CommonUtilities.FindElement(p0);
				if (welcome != Keywords.Constants["Welcome-message"])
					Assert.Fail();
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element"  + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Given(@"Click ""(.*)"" on the left navigaiton")]
		public void GivenClickOnTheLeftNavigaiton(string p0)
		{
			try
			{
				Thread.Sleep(10000);
				CommonUtilities.Click(p0);
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}


		[Then(@"Click ""(.*)"" on the welcome section")]
		public void ThenClickOnTheWelcomeSection(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element"  + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"select ""(.*)""from the dropdown")]
		public void ThenSelectFromTheDropdown(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element"  + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"I see ""(.*)"" on the welcome section")]
		public void ThenISeeOnTheWelcomeSection(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.FindElement(p0);
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		
		[Given(@"click ""(.*)"" button")]
		public void GivenClickButton(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				CommonUtilities.TakeScreenshot();

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"Click ""(.*)"" on the navigation section")]
		public void ThenClickOnTheNavigationSection(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(Keywords.Constants[p0]);
                Thread.Sleep(10000);
                CommonUtilities.TakeScreenshot();
            }
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();
			}
		}

		
		[Then(@"I enter ""(.*)"" into ""(.*)"" field")]
		public void ThenIEnterIntoField(string p0, string p1)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.EnterText(p1, p0);
                Thread.Sleep(1000);
                CommonUtilities.TakeScreenshot();

            }
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}


		[Then(@"Enter ""(.*)"" into ""(.*)"" field")]
		public void ThenEnterIntoField(string p0, string p1)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.EnterTextToChild(p1, p0);
                Thread.Sleep(1000);
                CommonUtilities.TakeScreenshot();

            }
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

        [Then(@"I Open USABB Tech Cases Dashboard")]
        public void ThenIOpenUSABBTechCasesDashboard()
        {
            try
            {
                Console.WriteLine("Opening url:" + Keywords.Constants["BPA_SITE_URL_Active"]);
                driver.Navigate().GoToUrl(Keywords.Constants["BPA_SITE_URL_Active"]);
                // Thread.Sleep(60 * 1000);
                CommonUtilities.TakeScreenshot();
            }
            catch (Exception ex)
            {

                CommonUtilities.TakeScreenshot();
                Console.WriteLine("UnableOpen USABB Tech Cases Dashboard" + ex.Message);
                CloseBrowser(false);
                Assert.Fail();

            }
        }



        [Then(@"I click ""(.*)""")]
		public void ThenIClick(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				Thread.Sleep(1000);
                CommonUtilities.TakeScreenshot();

            }
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}



		[Then(@"I select ""(.*)""")]
		public void ThenISelect(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.FindElement(p0);
				Thread.Sleep(1000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}


		[Then(@"I click ""(.*)"" button")]
		public void ThenIClickButton(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				Thread.Sleep(1000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}
		


		[Then(@"I click ""(.*)"" dropdown")]
		public void ThenIClickDropdown(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				Thread.Sleep(5000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"I select ""(.*)"" from the dropdown")]
		public void ThenISelectFromTheDropdown(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				PropertiesCollection.driver.FindElement(By.ClassName("clsSelecteDc")).SendKeys("AL04-Birmingham");
				PropertiesCollection.driver.FindElement(By.ClassName("clsSelecteDc")).SendKeys(Keys.Enter);
				Thread.Sleep(5000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"I click ""(.*)"" link")]
		public void ThenIClickLink(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				Thread.Sleep(5000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}
        		
		[Then(@"I wait for (.*) seconds")]
		public void ThenIWaitForSeconds(int p0)
		{
			
			Thread.Sleep(p0 * 1000);
		}

        [Then(@"I select ""(.*)"" from search")]
		public void ThenISelectFromSearch(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				PropertiesCollection.driver.FindElement(By.ClassName("clsUserName")).SendKeys("Arana, Richard");
				PropertiesCollection.driver.FindElement(By.ClassName("clsUserName")).SendKeys(Keys.Enter);
				Thread.Sleep(1000);

			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}
        
		public void ThenClickOnTheOnTheNavigationSection(string p0)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.Click(p0);
				CommonUtilities.TakeScreenshot();
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"I enter ""(.*)"" to ""(.*)"" field")]
		public void ThenIEnterToField(string p0, string p1)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.EnterText(p1, p0);
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}

		[Then(@"I select ""(.*)"" from ""(.*)"" dropdown")]
		public void ThenISelectFromDropdown(string p0, string p1)
		{
			try
			{
				Thread.Sleep(1000);
				CommonUtilities.SelectOption(p1, p0);
			}
			catch (Exception ex)
			{
				CommonUtilities.TakeScreenshot();
				Console.WriteLine("Unable to find element" + ex.Message);
				CloseBrowser(false);
				Assert.Fail();

			}
		}
        

		[Given(@"I close browser")]
		public void GivenICloseBrowser()
		{
			Thread.Sleep(1000);
			CommonUtilities.TakeScreenshot();
			CloseBrowser(false);
		}


	}
}
