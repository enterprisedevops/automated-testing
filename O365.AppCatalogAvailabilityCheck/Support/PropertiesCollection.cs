﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O365.AppCatalogAvailabilityCheck
{
    public enum ElementType
    {
        Id,
        Name,
        LinkText,
        CssName,
        ClassName,
        XPath,
        Model
    }
    class PropertiesCollection
    {
        public static IWebDriver driver { get; set; }
        //public static NgWebDriver ngDriver { get; set; }
    }
}
