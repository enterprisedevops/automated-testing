﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Drawing.Imaging;
using System.Security.Cryptography;
using System.Threading;
using System.Configuration;

namespace O365.AppCatalogAvailabilityCheck
{
    public class CommonUtilities
    {

        //Genric Support Methods
        public static void EnterText(string el, string val)
        {
			
			string element = Keywords.Dic[el];
			string value = Keywords.Constants[val];
			

			if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Model.ToString().ToLower())
            {
                //PropertiesCollection.driver.FindElement(NgBy.Model(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).Clear();
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).SendKeys(value);
            }
        }

        public static void EnterTextToChild(string el, string val)
        {
            ///Todo: Find element first else throw error
            string element = Keywords.Dic[el];
            string value = Keywords.Constants[val];
            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Model.ToString().ToLower())
            {
                //PropertiesCollection.driver.FindElement(NgBy.Model(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).FindElement(By.CssSelector("input[type = 'text']")).SendKeys(Keys.Backspace);
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).FindElement(By.CssSelector("input[type = 'text']")).SendKeys(Keys.Backspace);
                //PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).FindElement(By.CssSelector("input[type = 'text']")).Clear();
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).FindElement(By.CssSelector("input[type = 'text']")).SendKeys(value);
                //PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).FindElement(By.CssSelector("input[type = 'text']")).Click();
            }
        }
        public static void EnterFreeText(string el, string val)
        {
            ///Todo: Find element first else throw error
            string element = Keywords.Dic[el];
            string value = val;
            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30)).Until(ExpectedConditions.ElementExists((By.XPath(element.Split('|')[1]))));
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Model.ToString().ToLower())
            {
                //PropertiesCollection.ngDriver.FindElement(NgBy.Model(element.Split('|')[1])).SendKeys(value);
            }
        }

        public static void EnterEncryptedText(string el, string val)
        {
            ///Todo: Find element first else throw error
            string element = Keywords.Dic[el];
            string pwd = Keywords.Constants[val];
            string value = "";
            value = Decrypt(pwd.Split('|')[0]);
            value+="&" + Decrypt(pwd.Split('|')[1]);

            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                new WebDriverWait(PropertiesCollection.driver, TimeSpan.FromSeconds(30)).Until(ExpectedConditions.ElementExists((By.XPath(element.Split('|')[1]))));
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).SendKeys(value);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Model.ToString().ToLower())
            {
                //PropertiesCollection.ngDriver.FindElement(NgBy.Model(element.Split('|')[1])).SendKeys(value);
            }
        }

        public static void EnterTextValue(string element, ElementType elementtype, string val)
        {
            if (elementtype == ElementType.Id)
            {
                PropertiesCollection.driver.FindElement(By.Id(element)).SendKeys(val);
            }
            else if (elementtype == ElementType.Name)
            {
                PropertiesCollection.driver.FindElement(By.Name(element)).SendKeys(val);
            }
            else if (elementtype == ElementType.XPath)
            {
                PropertiesCollection.driver.FindElement(By.XPath(element)).SendKeys(val);
            }
        }

        public static void Click(string el)
        {
            string element = Keywords.Dic[el];
            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).Click();
            }
        }

       
        public static void PressKeys(string el)
        {
            string element = Keywords.Dic[el];
            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).SendKeys(Keys.Tab);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).SendKeys(Keys.Tab);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).SendKeys(Keys.Tab);
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).SendKeys(Keys.Tab);
            }
        }

        public static void SelectOption(string el, string value)
        {

            string element = Keywords.Dic[el];
            if (element.Split('|')[0] == ElementType.Id.ToString())
            {
                PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).Click();
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).Click();
                PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).SendKeys(value + Keys.Enter);
            }
            
        }
        public static void SelectDropdown(string element, string value, ElementType elementtype)
        {
            if (elementtype == ElementType.Id)
            {
                new SelectElement(PropertiesCollection.driver.FindElement(By.Id(element))).SelectByText(value);
            }
            else if (elementtype == ElementType.Name)
            {
                new SelectElement(PropertiesCollection.driver.FindElement(By.Name(element))).SelectByText(value);
            }
            else if (elementtype == ElementType.XPath)
            {
                new SelectElement(PropertiesCollection.driver.FindElement(By.XPath(element))).SelectByText(value);
            }
            else if (elementtype == ElementType.ClassName)
            {
                new SelectElement(PropertiesCollection.driver.FindElement(By.ClassName(element))).SelectByText(value);
            }
        }

        public static string GetText(string element, ElementType elementtype)
        {
            if (elementtype == ElementType.Id)
            {
                return PropertiesCollection.driver.FindElement(By.Id(element)).Text;
            }
            else if (elementtype == ElementType.Name)
            {
                return PropertiesCollection.driver.FindElement(By.Name(element)).Text;
            }
            else if (elementtype == ElementType.XPath)
            {
                return PropertiesCollection.driver.FindElement(By.XPath(element)).Text;
            }
            else return string.Empty;


        }

        public static string FindElement(string el)
        {

            string element = Keywords.Dic[el];
            if (element.Split('|')[0].ToLower() == ElementType.Id.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1])).Text;

            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1])).Text;
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElement(By.ClassName(element.Split('|')[1])).Text;
            }

            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1])).Text;
            }
            else return string.Empty;


        }

        public static IList<IWebElement> GetElements(string el)
        {

            string element = Keywords.Dic[el];
            if (element.Split('|')[0].ToLower() == ElementType.Id.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElements(By.Id(element.Split('|')[1]));

            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElements(By.Name(element.Split('|')[1]));
            }
            else if (element.Split('|')[0].ToLower() == ElementType.ClassName.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElements(By.ClassName(element.Split('|')[1]));
            }

            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElements(By.XPath(element.Split('|')[1]));
            }
            else return null;


        }


        public static IWebElement getWebElement(string el, string AddString)
        {
            
            string element = Keywords.Dic[el];
            
            if (element.Split('|')[0].ToLower() == ElementType.Id.ToString().ToLower())
            {
              return PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1]));

            }
            else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
            {
                return PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1]));
            }
            else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
            {   if(string.IsNullOrEmpty(AddString))
                    return PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1]));
                  else
                    return PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1] + AddString));
            }
            else return null;


        }




        public static bool IsElementPresent(string el)
        {
            bool isPrsent = true;
            try
            {
                string element = Keywords.Dic[el];
                if (element.Split('|')[0].ToLower() == ElementType.Id.ToString().ToLower())
                {
                    PropertiesCollection.driver.FindElement(By.Id(element.Split('|')[1]));
                    return isPrsent;

                }
                else if (element.Split('|')[0].ToLower() == ElementType.Name.ToString().ToLower())
                {
                    PropertiesCollection.driver.FindElement(By.Name(element.Split('|')[1]));
                    return isPrsent;
                }
                else if (element.Split('|')[0].ToLower() == ElementType.XPath.ToString().ToLower())
                {
                    PropertiesCollection.driver.FindElement(By.XPath(element.Split('|')[1]));
                    return isPrsent;
                }
                else return false;
            }
            catch
            {
                return false;
            }
        }


        public static void OpenUrl(string val)
        {
           
            try
            {
                string url = Keywords.Constants["SITE_URL_Active"] + "/#/" + Keywords.Constants[val];
                PropertiesCollection.driver.Navigate().GoToUrl(url); 
            }
            catch
            {
                
            }
        }
		public static string GetPONumber()
		{
			string po = "";
			string poDataReportPath = ConfigurationManager.AppSettings["PODataReportPathQA"] + @"\Inbound Delivery Report.xlsx";
			var ds = ExcelHelper.GetExcelDataSet(poDataReportPath);
			po = ds.Tables[0].Rows[0][4].ToString();
			return po;
		}

		#region Common utilities


		public static bool VerifyMouseOver(string elementName)
        {
            bool isValid = true;
            try
            {
                var element = getWebElement(elementName,string.Empty);
                Actions action = new Actions(PropertiesCollection.driver);
                action.MoveToElement(element).Perform();
                string bgcolor = element.GetCssValue("background-color");
                if(bgcolor== "rgba(136, 136, 136, 1)") return isValid;
                else return isValid = false;
            }
            catch { return isValid = false; }

        }

        public static bool VerifyDoubleArrow(string elementName)
        {
            bool isValid = true;
            try
            {
                var element = getWebElement(elementName, "/i");
                Actions action = new Actions(PropertiesCollection.driver);
                action.MoveToElement(element).Perform();
                string clsName = element.GetAttribute("class");
                if (clsName == "icon-table-arrow-double pull-right") return isValid;
                else return isValid = false;
            }
            catch { return isValid = false; }

        }

        public static bool VerifySortDirection(string elementName, string direction)
        {
            bool isValid = true;
            try
            {
               var element = getWebElement(elementName, "/i");                
                string clsName = element.GetAttribute("class");
                if (direction == "Up")
                {
                    if (clsName == "icon-table-arrow-up pull-right") return isValid;
                    else return isValid = false;
                }
                else
                {
                    if (clsName == "icon-table-arrow-down pull-right") return isValid;
                    else return isValid = false;
                }
            }
            catch { return isValid = false; }

        }
        public static bool VerifyTextAlign(string elementName)
        {
            bool isValid = true;
            try
            {
                var element = getWebElement(elementName, string.Empty);
                Actions action = new Actions(PropertiesCollection.driver);
                action.MoveToElement(element).Perform();
                string textAlign = element.GetCssValue("text-align");
                if (textAlign == "left") return isValid;
                else return isValid = false;
            }
            catch { return isValid = false; }

        }

        public static bool VerifySorting(string elementName, string sortDir)
        {
            bool isValid = true;
            try
            {
                var element = getWebElement(elementName, string.Empty);

                if (sortDir == "Up")
                {
                    element.Click();
                    Thread.Sleep(1000);
                    if (VerifySortDirection(elementName, sortDir))
                        Console.WriteLine(elementName + " -Asscending sort verified");
                    else
                        Console.WriteLine(elementName + " -Asscending sort verification failed");
                }
                else
                {
                    element.Click();
                    Thread.Sleep(1000);
                    if (VerifySortDirection(elementName, sortDir))
                        Console.WriteLine(elementName + " -Desscending sort verified");
                    else
                        Console.WriteLine(elementName + " -Desscending sort verification failed");
                }
                return isValid;
            }
            catch { return isValid = false; }

        }

        public static bool VerifySorting()
        {
            bool isValid = true;
            try
            {
                //ArrayList obtainedList = new ArrayList();
                //List<IWebElement> elementList = PropertiesCollection.driver.FindElements(By.Id("tableId));
                //for (IWebElement elm in elementList)
                //{
                //    obtainedList.Add(elm.Text);
                //}
                //ArrayList sortedList = new ArrayList();
                //for (string s in obtainedList)
                //{
                //    sortedList.Add(s);
                //}
                //sortedList.Sort();
                //Assert.assertTrue(sortedList.equals(obtainedList));
                return isValid;
            }
            catch { return isValid = false; }

        }

       
        public static void FilloutCardDetails()
        {
            try
            {
                PropertiesCollection.driver.FindElement(By.Id("cardBrand")).SendKeys("Visa");
                PropertiesCollection.driver.FindElement(By.Id("cardNumber")).SendKeys("4111111111111111");
                PropertiesCollection.driver.FindElement(By.Id("cardExpiryMonth")).SendKeys("02");
                PropertiesCollection.driver.FindElement(By.Id("cardExpiryYear")).SendKeys("21");
                PropertiesCollection.driver.FindElement(By.Id("cardVerificationCode")).SendKeys("121");
                PropertiesCollection.driver.FindElement(By.Id("cardHolderName")).SendKeys("Automated test");
                PropertiesCollection.driver.FindElement(By.Id("pin")).SendKeys("123");
                PropertiesCollection.driver.FindElement(By.Id("poNumber")).SendKeys("123456");
                
            }
            catch { }

        }
        public static string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while decryption: {0}", ex);
                return string.Empty;
            }
            return cipherText;
        }

      
         public static void TakeScreenshot()
        {
            try
            {
                string fileNameBase = string.Format("Screenshot_{0}", DateTime.Now.ToString("yyyyMMdd_HHmmss"));

                var artifactDirectory = @"\\gscfiles02.hsi.hughessupply.com\tfs-prd$\Reports\images\ibs\"; //Path.Combine(Directory.GetCurrentDirectory(), "ScreenShots");
                                                                                           //if (!Directory.Exists(artifactDirectory))
                                                                                           //    Directory.CreateDirectory(artifactDirectory);
                ITakesScreenshot takesScreenshot =PropertiesCollection.driver as ITakesScreenshot;

                if (takesScreenshot != null)
                {
                    var screenshot = takesScreenshot.GetScreenshot();

                    string screenshotFilePath = Path.Combine(artifactDirectory, fileNameBase + ".png");

                    screenshot.SaveAsFile(screenshotFilePath, ScreenshotImageFormat.Png);

                    Console.WriteLine("imgStart" + "http://GHDTFB02WPL.hds.hdsupply.com:9001/images/ibs/" + fileNameBase + ".png" + "imgEnd");

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
        }
        #endregion
    }
}
