﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O365.AppCatalogAvailabilityCheck
{
	public static class ExcelHelper
	{
		public static DataSet GetExcelDataSet(string path)
		{
			string sheetName;
			string ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path +
								  ";Extended Properties=Excel 12.0;";
			DataSet ds = new DataSet();
			using (OleDbConnection con = new OleDbConnection(ConnectionString))
			{
				using (OleDbCommand cmd = new OleDbCommand())
				{
					using (OleDbDataAdapter oda = new OleDbDataAdapter())
					{
						cmd.Connection = con;
						con.Open();
						DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

						sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
						sheetName = sheetName.Replace("'", "");
						DataTable dt = new DataTable(sheetName);
						cmd.Connection = con;
						cmd.CommandText = "SELECT top 1 * FROM [" + sheetName + "] WHERE [Receiving DC] = 'GA02'";
						oda.SelectCommand = cmd;
						oda.Fill(dt);
						dt.TableName = sheetName;
						ds.Tables.Add(dt);
					}
				}
			}
			return ds;
		}
	}
}
